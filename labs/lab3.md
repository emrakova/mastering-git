# Lab 3

* Fork this repo
* Clone your fork and set up origin and upstream remotes properly
* Contribute using a merge request
  * Create a new base branch for the merge request
* Open autumn2023.md and write down what’s the most valuable thing you learnt so far on this course
* Commit and push to your fork
* Open the merge request
* CI needs to pass

Tutor, do that :) Create an MR that fixes TBD's in the solution document.

BONUS: showcase `git pull` on a outdated branch: explain what is wrong with it
